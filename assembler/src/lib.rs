
use std::{error, fmt, io};
use std::rc::Rc;
use std::cell::RefCell;
use std::path::PathBuf;
use std::fs::File;
use std::io::{BufReader, BufRead, BufWriter, Write};
use std::num::ParseIntError;
use std::collections::HashMap;
use std::collections::hash_map::Entry;

use regex::Regex;

#[derive(Debug)]
pub enum AssemblerError {
    FileOpen(PathBuf, io::Error),
    FileRead(PathBuf, io::Error),
    FileWrite(PathBuf, io::Error),
    Syntax(usize, String),
    InvalidInstruction(usize, String),
    MissingOperand(usize, String),
    TooManyOperands(usize),
    InvalidRegister(usize, String),
    InvalidIntegerLiteral(usize, String, ParseIntError),
    InvalidCondition(usize, String),
    DuplicateLabel(usize, String),
    UndefinedLabel(String),
    AddressSpaceExhausted(),
}

impl fmt::Display for AssemblerError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        match self {
            AssemblerError::FileOpen(path, _) => write!(f, "unable to open {}", path.display()),
            AssemblerError::FileRead(path, _) => write!(f, "failed to read {}", path.display()),
            AssemblerError::FileWrite(path, _) => write!(f, "failed to write {}", path.display()),
            AssemblerError::Syntax(line_number, line) => write!(f, "syntax error at line {}: {}", line_number, line),
            AssemblerError::InvalidInstruction(line_number, instr) => write!(f, "invalid instruction \"{}\" at line {}", instr, line_number),
            AssemblerError::MissingOperand(line_number, name) => write!(f, "missing operand \"{}\" at line {}", name, line_number),
            AssemblerError::TooManyOperands(line_number) => write!(f, "too many operands at line {}", line_number),
            AssemblerError::InvalidRegister(line_number, name) => write!(f, "invalid register \"{}\" at line {}", name, line_number),
            AssemblerError::InvalidIntegerLiteral(line_number, literal, _) => write!(f, "invalid integer literal \"{}\" at line {}", literal, line_number),
            AssemblerError::InvalidCondition(line_number, instruction) => write!(f, "invalid condition \"{}\" at line {}", instruction, line_number),
            AssemblerError::DuplicateLabel(line_number, label) => write!(f, "duplicate label \"{}\" at line {}", label, line_number),
            AssemblerError::UndefinedLabel(label) => write!(f, "usage of undefined label \"{}\"", label),
            AssemblerError::AddressSpaceExhausted() => write!(f, "address space exhausted"),
        }
    }
}

impl error::Error for AssemblerError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            AssemblerError::FileOpen(_, io_error) => Some(io_error),
            AssemblerError::FileRead(_, io_error) => Some(io_error),
            AssemblerError::FileWrite(_, io_error) => Some(io_error),
            AssemblerError::InvalidIntegerLiteral(_, _, parse_error) => Some(parse_error),
            _ => None,
        }
    }
}

#[derive(Copy, Clone, Debug)]
enum OpCode {
    MOV = 0x00,
    LD = 0x01,
    LDI = 0x11,
    ST = 0x02,
    AND = 0x03,
    ANDI = 0x13,
    OR = 0x04,
    ORI = 0x14,
    XOR = 0x05,
    XORI = 0x15,
    NOT = 0x06,
    ADD = 0x07,
    ADDI = 0x17,
    SUB = 0x08,
    SUBI = 0x18,
    SL = 0x09,
    SLI = 0x19,
    SR = 0x0A,
    SRI = 0x1A,
    CMP = 0x0B,
    CMPI = 0x1B,
    JMP = 0x0C,
}

#[derive(Debug)]
struct ImmediateValue {
    value: Option<u16>,
    label_name: Option<String>,
}

impl ImmediateValue {
    fn new_const(value: u16) -> ImmediateValue {
        ImmediateValue {
            value: Some(value),
            label_name: None,
        }
    }

    fn new_label_reference(label_name: String) -> ImmediateValue {
        ImmediateValue {
            value: None,
            label_name: Some(label_name),
        }
    }

    fn get(&self) -> Option<u16> {
        self.value
    }

    fn update(&mut self, labels: &HashMap<String, Rc<RefCell<Label>>>) -> Result<(), AssemblerError> {
        if let Some(label_name) = &self.label_name {
            let label_refcell = labels.get(label_name)
                .ok_or_else(|| AssemblerError::UndefinedLabel(label_name.to_string()))?;

            let label = label_refcell.borrow();

            self.value = Some(label.address);
        }

        Ok(())
    }
}

#[derive(Copy, Clone, Debug)]
enum Register {
    R0 = 0x0,
    R1 = 0x1,
    R2 = 0x2,
    Scratch = 0x3,
    SP = 0x4,
    RAddr = 0x5,
    SR = 0x6,
    PC = 0x7,
}

impl Register {
    fn from_str(reg_str: &str) -> Option<Register> {
        let reg_str = reg_str.to_lowercase();
        match reg_str.as_str() {
            "r0" => Some(Register::R0),
            "r1" => Some(Register::R1),
            "r2" => Some(Register::R2),
            "scratch" => Some(Register::Scratch),
            "sp" => Some(Register::SP),
            "raddr" => Some(Register::RAddr),
            "sr" => Some(Register::SR),
            "pc" => Some(Register::PC),
            _ => None,
        }
    }

    fn encode(&self) -> u16 {
        *self as u16
    }
}

#[derive(Copy, Clone, Debug)]
enum Condition {
    None = 0x0,
    Zero = 0x1,
    Equal = 0x2,
    NotEqual = 0x3,
    LessThan = 0x4,
    LessThanOrEqual = 0x5,
    GreaterThan = 0x6,
    GreaterThanOrEqual = 0x7,
}

impl Condition {
    fn from_str(cond_str: &str) -> Option<Condition> {
        let reg_str = cond_str.to_lowercase();
        match reg_str.as_str() {
            "z" => Some(Condition::Zero),
            "eq" => Some(Condition::Equal),
            "ne" => Some(Condition::NotEqual),
            "lt" => Some(Condition::LessThan),
            "le" => Some(Condition::LessThanOrEqual),
            "gt" => Some(Condition::GreaterThan),
            "ge" => Some(Condition::GreaterThanOrEqual),
            _ => None,
        }
    }

    fn encode(&self) -> u16 {
        *self as u16
    }
}

trait AssemblyItem : fmt::Debug {
    fn size(&self) -> usize;
    fn update(&mut self, address: u16, labels: &HashMap<String, Rc<RefCell<Label>>>) -> Result<bool, AssemblerError>;
    fn to_instructions(&self) -> Vec<Instruction>;
}

#[allow(dead_code)]
#[derive(Copy, Clone, Debug)]
enum Instruction {
    NoData(OpCode),
    Immediate1(OpCode, u16),
    Register1(OpCode, Register),
    Register2(OpCode, Register, Register),
    Register3(OpCode, Register, Register, Register),
    Jump(OpCode, Condition, Register, bool),
}

impl Instruction {
    fn new_mov(target: Register, source: Register) -> Instruction { Instruction::Register2(OpCode::MOV, target, source) }
    fn new_ld(target: Register, source_address: Register) -> Instruction { Instruction::Register2(OpCode::LD, target, source_address) }
    fn new_ldi(value: u16) -> Instruction { Instruction::Immediate1(OpCode::LDI, value) }
    fn new_st(target_address: Register, source: Register) -> Instruction { Instruction::Register2(OpCode::ST, target_address, source) }
    fn new_and(target: Register, operand1: Register, operand2: Register) -> Instruction { Instruction::Register3(OpCode::AND, target, operand1, operand2) }
    fn new_andi(value: u16) -> Instruction { Instruction::Immediate1(OpCode::ANDI, value) }
    fn new_or(target: Register, operand1: Register, operand2: Register) -> Instruction { Instruction::Register3(OpCode::OR, target, operand1, operand2) }
    fn new_ori(value: u16) -> Instruction { Instruction::Immediate1(OpCode::ORI, value) }
    fn new_xor(target: Register, operand1: Register, operand2: Register) -> Instruction { Instruction::Register3(OpCode::XOR, target, operand1, operand2) }
    fn new_xori(value: u16) -> Instruction { Instruction::Immediate1(OpCode::XORI, value) }
    fn new_not(target: Register, operand1: Register) -> Instruction { Instruction::Register2(OpCode::NOT, target, operand1) }
    fn new_add(target: Register, operand1: Register, operand2: Register) -> Instruction { Instruction::Register3(OpCode::ADD, target, operand1, operand2) }
    fn new_addi(value: u16) -> Instruction { Instruction::Immediate1(OpCode::ADDI, value) }
    fn new_sub(target: Register, operand1: Register, operand2: Register) -> Instruction { Instruction::Register3(OpCode::SUB, target, operand1, operand2) }
    fn new_subi(value: u16) -> Instruction { Instruction::Immediate1(OpCode::SUBI, value) }
    fn new_sl(target: Register, operand1: Register, operand2: Register) -> Instruction { Instruction::Register3(OpCode::SL, target, operand1, operand2) }
    fn new_sli(value: u16) -> Instruction { Instruction::Immediate1(OpCode::SLI, value) }
    fn new_sr(target: Register, operand1: Register, operand2: Register) -> Instruction { Instruction::Register3(OpCode::SR, target, operand1, operand2) }
    fn new_sri(value: u16) -> Instruction { Instruction::Immediate1(OpCode::SRI, value) }
    fn new_cmp(operand1: Register, operand2: Register) -> Instruction { Instruction::Register2(OpCode::CMP, operand1, operand2) }
    fn new_cmpi(value: u16) -> Instruction { Instruction::Immediate1(OpCode::CMPI, value) }
    fn new_jmp(cond: Condition, address: Register, link: bool) -> Instruction { Instruction::Jump(OpCode::JMP, cond, address, link) }

    fn encode(&self) -> u16 {
        match self {
            Instruction::NoData(opcode) => (*opcode as u16) << 11,
            Instruction::Immediate1(opcode, value) => (*opcode as u16) << 11 | *value,
            Instruction::Register1(opcode, reg) => (*opcode as u16) << 11 | reg.encode(),
            Instruction::Register2(opcode, reg1, reg2) => (*opcode as u16) << 11 | reg2.encode() << 3 | reg1.encode(),
            Instruction::Register3(opcode, reg1, reg2, reg3) => (*opcode as u16) << 11 | reg3.encode() << 6 | reg2.encode() << 3 | reg1.encode(),
            Instruction::Jump(opcode, cond, reg, link) => (*opcode as u16) << 11 | (*link as u16) << 6 | reg.encode() << 3 | cond.encode(),
        }
    }
}

impl AssemblyItem for Instruction {
    fn size(&self) -> usize { 1 }
    fn update(&mut self, _address: u16, _labels: &HashMap<String, Rc<RefCell<Label>>>) -> Result<bool, AssemblerError> { Ok(false) }
    fn to_instructions(&self) -> Vec<Instruction> { vec![*self] }
}

#[derive(Debug)]
struct LDIPseudoInstruction {
    value: ImmediateValue,
    instruction_count: usize,
}

fn generate_load_immediate(value: u16, minimum_count: usize) -> Vec<Instruction> {
    let leading_zeros = value.leading_zeros();
    let trailing_zeros = if value == 0 { 0 } else { value.trailing_zeros() };
    let actual_width = 16 - trailing_zeros - leading_zeros;

    if minimum_count <= 1 && leading_zeros >= 16 - 11 {
        // Not using the upper bits
        vec![
            Instruction::new_ldi(value),
        ]
    } else if minimum_count <= 2 && actual_width <= 11 {
        // Using the upper bits but still less than 11 bits
        vec![
            Instruction::new_ldi(value >> trailing_zeros),
            Instruction::new_sli(trailing_zeros as u16),
        ]
    } else if minimum_count <= 2 && value & 0xf800 == 0xf800 {
        // Bits at positions >= 11 are all 1
        vec![
            Instruction::new_ldi(!value),
            Instruction::new_not(Register::R0, Register::R0),
        ]
    } else if minimum_count <= 3 {
        // Full 16 bits
        vec![
            Instruction::new_ldi((value & 0xff00) >> 8),
            Instruction::new_sli(8),
            Instruction::new_ori(value & 0x00ff),
        ]
    } else {
        panic!("Refusing to generate LDI with more than 3 instructions")
    }
}

impl LDIPseudoInstruction {
    fn new(value: ImmediateValue) -> LDIPseudoInstruction {
        LDIPseudoInstruction {
            value,
            instruction_count: 1
        }
    }
}

impl AssemblyItem for LDIPseudoInstruction {
    fn size(&self) -> usize {
        self.instruction_count
    }

    fn update(&mut self, _address: u16, labels: &HashMap<String, Rc<RefCell<Label>>>) -> Result<bool, AssemblerError> {
        self.value.update(labels)?;

        let value = self.value.get().unwrap();

        let old_instruction_count = self.instruction_count;

        let instructions = generate_load_immediate(value, self.instruction_count);

        self.instruction_count = instructions.len();

        Ok(self.instruction_count != old_instruction_count)
    }

    fn to_instructions(&self) -> Vec<Instruction> {
        generate_load_immediate(self.value.get().unwrap(), self.instruction_count)
    }
}

#[derive(Debug)]
struct Label {
    name: String,
    address: u16,
}

impl Label {
    fn new(name: String, address: u16) -> Label {
        Label {
            name,
            address,
        }
    }
}

impl AssemblyItem for Label {
    fn size(&self) -> usize { 0 }

    fn update(&mut self, address: u16, _labels: &HashMap<String, Rc<RefCell<Label>>>) -> Result<bool, AssemblerError> {
        self.address = address;

        Ok(false)
    }

    fn to_instructions(&self) -> Vec<Instruction> { vec![] }
}

fn parse_u16(text: &str) -> Result<u16, ParseIntError> {
    if text.starts_with("0x") {
        u16::from_str_radix(&text["0x".len()..], 16)
    } else if text.starts_with("0o") {
        u16::from_str_radix(&text["0o".len()..], 8)
    } else if text.starts_with("0b") {
        u16::from_str_radix(&text["0b".len()..], 2)
    } else {
        u16::from_str_radix(text, 10)
    }
}

pub fn run(source_path: PathBuf, output_path: PathBuf) -> Result<(), AssemblerError> {
    let source_file = File::open(&source_path)
        .map_err(|err| AssemblerError::FileOpen(source_path.clone(), err))?;

    let source_reader = BufReader::new(source_file);

    let mut items: Vec<Rc<RefCell<dyn AssemblyItem>>> = Vec::new();

    let mut labels_map = HashMap::new();

    let re = Regex::new(r"(?x)
^\s*  # Start of line
(?:([a-zA-Z_]\w*)\s*:\s*)?  # Optional label
# Instruction (optional as lines may also contain only a label)
(?:
  ([a-zA-Z.]+)  # Opcode
  # Operands
  (?:
    \s+
    # Operand 1
    (\w+)
    # Operand 2
    (?:\s*,\s*(\w+))?
    # Operand 3
    (?:\s*,\s*(\w+))?
  )?
)?
\s*
(?:\#.*)?$  # Comment and end of line
").unwrap();

    let mut current_address: u16 = 0;
    for (line_number, line) in source_reader.lines().enumerate() {
        let line = line
            .map_err(|err| AssemblerError::FileRead(source_path.clone(), err))?;

        let m = re.captures(&line);

        if let Some(captures) = m {
            let label = captures.get(1).map(|m| m.as_str());
            let instruction = captures.get(2).map(|m| m.as_str());
            let operand1 = captures.get(3).map(|m| m.as_str());
            let operand2 = captures.get(4).map(|m| m.as_str());
            let operand3 = captures.get(5).map(|m| m.as_str());

            if let Some(label_name) = label {
                match labels_map.entry(label_name.to_string()) {
                    Entry::Occupied(_) => {
                        return Err(AssemblerError::DuplicateLabel(line_number, label_name.to_string()));
                    },
                    Entry::Vacant(v) => {
                        let label = Rc::new(RefCell::new(Label::new(label_name.to_string(), current_address)));
                        v.insert(label.clone());
                        items.push(label);
                    },
                }
            }

            if let Some(full_instruction_str) = instruction {
                let mut instruction_parts = full_instruction_str.splitn(2, '.');
                let instruction_str = instruction_parts.next().unwrap().to_lowercase();
                let condition_str = instruction_parts.next();

                let instr: Rc<RefCell<dyn AssemblyItem>> = match instruction_str.as_str() {
                    "mov" => {
                        let target_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "target register".to_string()))?;
                        let source_str = operand2
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register".to_string()))?;
                        if operand3.is_some() {
                            return Err(AssemblerError::TooManyOperands(line_number));
                        }

                        let target = Register::from_str(target_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, target_str.to_string()))?;
                        let source = Register::from_str(source_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source_str.to_string()))?;

                        Rc::new(RefCell::new(Instruction::new_mov(target, source)))
                    },
                    "ld" => {
                        let target_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "target register".to_string()))?;
                        let source_str = operand2
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source address register".to_string()))?;
                        if operand3.is_some() {
                            return Err(AssemblerError::TooManyOperands(line_number));
                        }

                        let target = Register::from_str(target_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, target_str.to_string()))?;
                        let source = Register::from_str(source_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source_str.to_string()))?;

                        Rc::new(RefCell::new(Instruction::new_ld(target, source)))
                    },
                    "ldi" => {
                        let value_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "value".to_string()))?;
                        if operand2.is_some() {
                            return Err(AssemblerError::TooManyOperands(line_number));
                        }

                        let first_char = value_str.chars().next().unwrap();

                        let data = if first_char.is_alphabetic() {
                            // Label
                            ImmediateValue::new_label_reference(value_str.to_string())
                        } else {
                            // Constant
                            let value = parse_u16(value_str)
                                .map_err(|err| AssemblerError::InvalidIntegerLiteral(line_number, value_str.to_string(), err))?;
                            ImmediateValue::new_const(value)
                        };

                        Rc::new(RefCell::new(LDIPseudoInstruction::new(data)))
                    },
                    "st" => {
                        let target_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "target address register".to_string()))?;
                        let source_str = operand2
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register".to_string()))?;
                        if operand3.is_some() {
                            return Err(AssemblerError::TooManyOperands(line_number));
                        }

                        let target = Register::from_str(target_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, target_str.to_string()))?;
                        let source = Register::from_str(source_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source_str.to_string()))?;

                        Rc::new(RefCell::new(Instruction::new_st(target, source)))
                    },
                    "and" => {
                        let target_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "target register".to_string()))?;
                        let source1_str = operand2
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register 1".to_string()))?;
                        let source2_str = operand3
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register 2".to_string()))?;

                        let target = Register::from_str(target_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, target_str.to_string()))?;
                        let source1 = Register::from_str(source1_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source1_str.to_string()))?;
                        let source2 = Register::from_str(source2_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source2_str.to_string()))?;

                        Rc::new(RefCell::new(Instruction::new_and(target, source1, source2)))
                    },
                    "andi" => {
                        let value_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "value".to_string()))?;
                        if operand2.is_some() {
                            return Err(AssemblerError::TooManyOperands(line_number));
                        }

                        let value = parse_u16(value_str)
                            .map_err(|err| AssemblerError::InvalidIntegerLiteral(line_number, value_str.to_string(), err))?;

                        Rc::new(RefCell::new(Instruction::new_andi(value)))
                    },
                    "or" => {
                        let target_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "target register".to_string()))?;
                        let source1_str = operand2
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register 1".to_string()))?;
                        let source2_str = operand3
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register 2".to_string()))?;

                        let target = Register::from_str(target_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, target_str.to_string()))?;
                        let source1 = Register::from_str(source1_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source1_str.to_string()))?;
                        let source2 = Register::from_str(source2_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source2_str.to_string()))?;

                        Rc::new(RefCell::new(Instruction::new_or(target, source1, source2)))
                    },
                    "ori" => {
                        let value_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "value".to_string()))?;
                        if operand2.is_some() {
                            return Err(AssemblerError::TooManyOperands(line_number));
                        }

                        let value = parse_u16(value_str)
                            .map_err(|err| AssemblerError::InvalidIntegerLiteral(line_number, value_str.to_string(), err))?;

                        Rc::new(RefCell::new(Instruction::new_ori(value)))
                    },
                    "xor" => {
                        let target_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "target register".to_string()))?;
                        let source1_str = operand2
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register 1".to_string()))?;
                        let source2_str = operand3
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register 2".to_string()))?;

                        let target = Register::from_str(target_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, target_str.to_string()))?;
                        let source1 = Register::from_str(source1_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source1_str.to_string()))?;
                        let source2 = Register::from_str(source2_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source2_str.to_string()))?;

                        Rc::new(RefCell::new(Instruction::new_xor(target, source1, source2)))
                    },
                    "xori" => {
                        let value_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "value".to_string()))?;
                        if operand2.is_some() {
                            return Err(AssemblerError::TooManyOperands(line_number));
                        }

                        let value = parse_u16(value_str)
                            .map_err(|err| AssemblerError::InvalidIntegerLiteral(line_number, value_str.to_string(), err))?;

                        Rc::new(RefCell::new(Instruction::new_xori(value)))
                    },
                    "not" => {
                        let target_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "target register".to_string()))?;
                        let source_str = operand2
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register".to_string()))?;
                        if operand3.is_some() {
                            return Err(AssemblerError::TooManyOperands(line_number));
                        }

                        let target = Register::from_str(target_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, target_str.to_string()))?;
                        let source = Register::from_str(source_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source_str.to_string()))?;

                        Rc::new(RefCell::new(Instruction::new_not(target, source)))
                    },
                    "add" => {
                        let target_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "target register".to_string()))?;
                        let source1_str = operand2
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register 1".to_string()))?;
                        let source2_str = operand3
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register 2".to_string()))?;

                        let target = Register::from_str(target_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, target_str.to_string()))?;
                        let source1 = Register::from_str(source1_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source1_str.to_string()))?;
                        let source2 = Register::from_str(source2_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source2_str.to_string()))?;

                        Rc::new(RefCell::new(Instruction::new_add(target, source1, source2)))
                    },
                    "addi" => {
                        let value_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "value".to_string()))?;
                        if operand2.is_some() {
                            return Err(AssemblerError::TooManyOperands(line_number));
                        }

                        let value = parse_u16(value_str)
                            .map_err(|err| AssemblerError::InvalidIntegerLiteral(line_number, value_str.to_string(), err))?;

                        Rc::new(RefCell::new(Instruction::new_addi(value)))
                    },
                    "sub" => {
                        let target_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "target register".to_string()))?;
                        let source1_str = operand2
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register 1".to_string()))?;
                        let source2_str = operand3
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register 2".to_string()))?;

                        let target = Register::from_str(target_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, target_str.to_string()))?;
                        let source1 = Register::from_str(source1_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source1_str.to_string()))?;
                        let source2 = Register::from_str(source2_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source2_str.to_string()))?;

                        Rc::new(RefCell::new(Instruction::new_sub(target, source1, source2)))
                    },
                    "subi" => {
                        let value_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "value".to_string()))?;
                        if operand2.is_some() {
                            return Err(AssemblerError::TooManyOperands(line_number));
                        }

                        let value = parse_u16(value_str)
                            .map_err(|err| AssemblerError::InvalidIntegerLiteral(line_number, value_str.to_string(), err))?;

                        Rc::new(RefCell::new(Instruction::new_subi(value)))
                    },
                    "sl" => {
                        let target_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "target register".to_string()))?;
                        let source1_str = operand2
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register 1".to_string()))?;
                        let source2_str = operand3
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register 2".to_string()))?;

                        let target = Register::from_str(target_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, target_str.to_string()))?;
                        let source1 = Register::from_str(source1_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source1_str.to_string()))?;
                        let source2 = Register::from_str(source2_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source2_str.to_string()))?;

                        Rc::new(RefCell::new(Instruction::new_sl(target, source1, source2)))
                    },
                    "sli" => {
                        let value_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "value".to_string()))?;
                        if operand2.is_some() {
                            return Err(AssemblerError::TooManyOperands(line_number));
                        }

                        let value = parse_u16(value_str)
                            .map_err(|err| AssemblerError::InvalidIntegerLiteral(line_number, value_str.to_string(), err))?;

                        Rc::new(RefCell::new(Instruction::new_sli(value)))
                    },
                    "sr" => {
                        let target_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "target register".to_string()))?;
                        let source1_str = operand2
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register 1".to_string()))?;
                        let source2_str = operand3
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register 2".to_string()))?;

                        let target = Register::from_str(target_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, target_str.to_string()))?;
                        let source1 = Register::from_str(source1_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source1_str.to_string()))?;
                        let source2 = Register::from_str(source2_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source2_str.to_string()))?;

                        Rc::new(RefCell::new(Instruction::new_sr(target, source1, source2)))
                    },
                    "sri" => {
                        let value_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "value".to_string()))?;
                        if operand2.is_some() {
                            return Err(AssemblerError::TooManyOperands(line_number));
                        }

                        let value = parse_u16(value_str)
                            .map_err(|err| AssemblerError::InvalidIntegerLiteral(line_number, value_str.to_string(), err))?;

                        Rc::new(RefCell::new(Instruction::new_sri(value)))
                    },
                    "cmp" => {
                        let source1_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register 1".to_string()))?;
                        let source2_str = operand2
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "source register 2".to_string()))?;
                        if operand3.is_some() {
                            return Err(AssemblerError::TooManyOperands(line_number));
                        }

                        let source1 = Register::from_str(source1_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source1_str.to_string()))?;
                        let source2 = Register::from_str(source2_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, source2_str.to_string()))?;

                        Rc::new(RefCell::new(Instruction::new_cmp(source1, source2)))
                    },
                    "cmpi" => {
                        let value_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "value".to_string()))?;
                        if operand2.is_some() {
                            return Err(AssemblerError::TooManyOperands(line_number));
                        }

                        let value = parse_u16(value_str)
                            .map_err(|err| AssemblerError::InvalidIntegerLiteral(line_number, value_str.to_string(), err))?;

                        Rc::new(RefCell::new(Instruction::new_cmpi(value)))
                    },
                    jmp_str @ "jmp" |
                    jmp_str @ "jal" => {
                        let link = jmp_str == "jal";

                        let cond = if let Some(condition_str) = condition_str {
                            Condition::from_str(condition_str)
                                .ok_or_else(|| AssemblerError::InvalidCondition(line_number, condition_str.to_string()))?
                        } else {
                            Condition::None
                        };

                        let address_str = operand1
                            .ok_or_else(|| AssemblerError::MissingOperand(line_number, "address register".to_string()))?;
                        if operand2.is_some() {
                            return Err(AssemblerError::TooManyOperands(line_number));
                        }

                        let address = Register::from_str(address_str)
                            .ok_or_else(|| AssemblerError::InvalidRegister(line_number, address_str.to_string()))?;

                        Rc::new(RefCell::new(Instruction::new_jmp(cond, address, link)))
                    },
                    _ => return Err(AssemblerError::InvalidInstruction(line_number, instruction_str.clone())),
                };

                {
                    let instr_ref = instr.borrow();

                    current_address = current_address.checked_add(instr_ref.size() as u16)
                        .ok_or_else(|| AssemblerError::AddressSpaceExhausted())?;
                }

                items.push(instr);
            }
        } else {
            return Err(AssemblerError::Syntax(line_number, line));
        }
    }

    loop {
        let mut changed = false;

        let mut address: u16 = 0;
        for item_refcell in &items {
            let mut item = item_refcell.borrow_mut();

            let size_changed = item.update(address, &labels_map)?;

            if size_changed {
                changed = true;
            }

            address = address.checked_add(item.size() as u16)
                .ok_or_else(|| AssemblerError::AddressSpaceExhausted())?;
        }

        if !changed {
            break;
        }
    }

    let instructions = items.iter()
        .flat_map(|item| item.borrow().to_instructions())
        .collect::<Vec<_>>();

    let output_file = File::create(&output_path)
        .map_err(|err| AssemblerError::FileOpen(output_path.clone(), err))?;

    let mut output_writer = BufWriter::new(output_file);

    for instruction in &instructions {
        let encoded = instruction.encode();
        let encoded_bytes = encoded.to_le_bytes();
        output_writer.write_all(&encoded_bytes)
            .map_err(|err| AssemblerError::FileWrite(output_path.clone(), err))?;
    }

    Ok(())
}
