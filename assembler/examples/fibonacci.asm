
# Initialize stack pointer to point to the first address of RAM
ldi 0x0400
mov SP, R0

# R2 is F1
ldi 1
mov R2, R0

# R1 is F0
ldi 0
mov R1, R0

loop:
    # Call display() with F0
    ldi display
    jal R0

    # Calculate next value: R1(n) = R2(n-1) and R2(n) = R1(n-1) + R2(n-1)
    add R0, R1, R2
    mov R1, R2
    mov R2, R0

    # Repeat
    ldi loop
    jmp R0

# Output a value on the GPIO port
# void display(int value)
display:
    # Preserve R0
    mov Scratch, R0

    # Write the value to the GPIO port
    ldi 0x0600
    st R0, R1

    # Restore R0
    mov R0, Scratch

    # Return
    jmp RAddr
