
start:

# Initialize stack pointer to point to the first address of RAM
ldi 0x0400
mov SP, R0

# Store 0xf0f0 on top of the stack
ldi 0xf0f0
st SP, R0

# Increment stack pointer
mov R0, SP
addi 1
mov SP, R0

# Store 0x0f0f on top of the stack
ldi 0x0f0f
st SP, R0

# Jump to subroutine
ldi subroutine
jal R0

# The subroutine jumps here when it is done

# Repeat the whole program
ldi start
jmp R0

subroutine:
    # Load value on top of the stack
    ld R1, SP

    # Output the value on the GPIO port
    ldi 0x0600
    st R0, R1

    # Load value at SP - 1
    mov R0, SP
    subi 1
    ld R1, R0

    # Output the value on the GPIO port
    ldi 0x0600
    st R0, R1

    # Return
    jmp RAddr
