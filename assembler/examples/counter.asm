
# This program waits until the button connected to the first GPIO pin is pressed. It then counts from 0 to 9 and
# displays the current value using the LEDs connected to the GPIO. Afterwards it restarts and waits until the button is
# pressed again.

start:

# Clear display
ldi 0x0600
mov R2, R0
ldi 0
st R2, R0

# Busy wait until button pressed
wait_button:
    # Load address of input peripheral
    ldi 0x0601
    # Read buttons
    ld R0, R0
    # Mask first button
    andi 0x0001
    # Repeat if button is not pressed
    ldi wait_button
    jmp.z R0

# Light up the whole display
ldi 0x0600
mov R2, R0
ldi 0xffff
st R2, R0

# Set the counter in R1 to 0
ldi 0
mov R1, R0

# Count from 0 to 9
loop:
    # Load address of display peripheral
    ldi 0x0600
    mov R2, R0
    # Copy counter from R1
    mov R0, R1
    # Display counter
    st R2, R0
    # Increment R0
    addi 1
    # Compare R0 to 10
    cmpi 10
    # Store counter in R1
    mov R1, R0
    # Load loop start address
    ldi loop
    # If R0 < 10, repeat
    jmp.lt R0

# Repeat
ldi start
jmp R0
