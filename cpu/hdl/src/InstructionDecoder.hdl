
block InstructionDecoder {
    in[16] instruction;
    out[11] immediate_value;
    out reg_in_valid;
    out[3] reg_in;
    out RAddr_in_PC;
    out SR_carry_in, SR_zero_in, SR_equal_in, SR_less_than_in;
    out immediate_out;
    out reg_out_valid;
    out[3] reg_out;
    out reg_out_addr_valid;
    out[3] reg_out_addr;
    out reg_out_operand1_valid;
    out[3] reg_out_operand1;
    out immediate_out_operand2;
    out reg_out_operand2_valid;
    out[3] reg_out_operand2;
    out[3] jump_condition;
    out alu_out;
    out[3] alu_fn;
    out mem_read, mem_write;

    wire[5] opcode;
    wire is_mov;
    wire is_ld;
    wire is_ldi;
    wire is_st;
    wire is_and;
    wire is_andi;
    wire is_or;
    wire is_ori;
    wire is_xor;
    wire is_xori;
    wire is_not;
    wire is_add;
    wire is_addi;
    wire is_sub;
    wire is_subi;
    wire is_sl;
    wire is_sli;
    wire is_sr;
    wire is_sri;
    wire is_cmp;
    wire is_cmpi;
    wire is_jmp;
    wire[3] jmp_cond;
    wire jmp_is_link;

    opcode = instruction[16:11];

    is_mov = ~opcode[0] & ~opcode[1] & ~opcode[2] & ~opcode[3] & ~opcode[4];
    is_ld = opcode[0] & ~opcode[1] & ~opcode[2] & ~opcode[3] & ~opcode[4];
    is_ldi = opcode[0] & ~opcode[1] & ~opcode[2] & ~opcode[3] & opcode[4];
    is_st = ~opcode[0] & opcode[1] & ~opcode[2] & ~opcode[3] & ~opcode[4];
    is_and = opcode[0] & opcode[1] & ~opcode[2] & ~opcode[3] & ~opcode[4];
    is_andi = opcode[0] & opcode[1] & ~opcode[2] & ~opcode[3] & opcode[4];
    is_or = ~opcode[0] & ~opcode[1] & opcode[2] & ~opcode[3] & ~opcode[4];
    is_ori = ~opcode[0] & ~opcode[1] & opcode[2] & ~opcode[3] & opcode[4];
    is_xor = opcode[0] & ~opcode[1] & opcode[2] & ~opcode[3] & ~opcode[4];
    is_xori = opcode[0] & ~opcode[1] & opcode[2] & ~opcode[3] & opcode[4];
    is_not = ~opcode[0] & opcode[1] & opcode[2] & ~opcode[3] & ~opcode[4];
    is_add = opcode[0] & opcode[1] & opcode[2] & ~opcode[3] & ~opcode[4];
    is_addi = opcode[0] & opcode[1] & opcode[2] & ~opcode[3] & opcode[4];
    is_sub = ~opcode[0] & ~opcode[1] & ~opcode[2] & opcode[3] & ~opcode[4];
    is_subi = ~opcode[0] & ~opcode[1] & ~opcode[2] & opcode[3] & opcode[4];
    is_sl = opcode[0] & ~opcode[1] & ~opcode[2] & opcode[3] & ~opcode[4];
    is_sli = opcode[0] & ~opcode[1] & ~opcode[2] & opcode[3] & opcode[4];
    is_sr = ~opcode[0] & opcode[1] & ~opcode[2] & opcode[3] & ~opcode[4];
    is_sri = ~opcode[0] & opcode[1] & ~opcode[2] & opcode[3] & opcode[4];
    is_cmp = opcode[0] & opcode[1] & ~opcode[2] & opcode[3] & ~opcode[4];
    is_cmpi = opcode[0] & opcode[1] & ~opcode[2] & opcode[3] & opcode[4];
    is_jmp = ~opcode[0] & ~opcode[1] & opcode[2] & opcode[3] & ~opcode[4];

    immediate_value = instruction[11:0];

    // mov
    reg_in_valid = 1#1 & is_mov;
    reg_in = instruction[3:0] & is_mov;
    reg_out_valid = 1#1 & is_mov;
    reg_out = instruction[6:3] & is_mov;

    // ld
    mem_read = 1#1 & is_ld;

    reg_in_valid = 1#1 & is_ld;
    reg_in = instruction[3:0] & is_ld;
    reg_out_addr_valid = 1#1 & is_ld;
    reg_out_addr = instruction[6:3] & is_ld;

    // ldi
    reg_in_valid = 1#1 & is_ldi;
    reg_in = 0#3 & is_ldi;
    immediate_out = 1#1 & is_ldi;

    // st
    mem_write = 1#1 & is_st;

    reg_out_addr_valid = 1#1 & is_st;
    reg_out_addr = instruction[3:0] & is_st;
    reg_out_valid = 1#1 & is_st;
    reg_out = instruction[6:3] & is_st;

    // and, andi
    SR_zero_in = 1#1 & (is_and | is_andi);
    alu_fn = 0#3 & (is_and | is_andi);
    alu_out = 1#1 & (is_and | is_andi);

    reg_in_valid = 1#1 & is_andi;
    reg_in = 0#3 & is_andi;
    reg_out_operand1_valid = 1#1 & is_andi;
    reg_out_operand1 = 0#3 & is_andi;
    immediate_out_operand2 = 1#1 & is_andi;

    reg_in_valid = 1#1 & is_and;
    reg_in = instruction[3:0] & is_and;
    reg_out_operand1_valid = 1#1 & is_and;
    reg_out_operand1 = instruction[6:3] & is_and;
    reg_out_operand2_valid = 1#1 & is_and;
    reg_out_operand2 = instruction[9:6] & is_and;

    // or, ori
    SR_zero_in = 1#1 & (is_or | is_ori);
    alu_fn = 1#3 & (is_or | is_ori);
    alu_out = 1#1 & (is_or | is_ori);

    reg_in_valid = 1#1 & is_ori;
    reg_in = 0#3 & is_ori;
    reg_out_operand1_valid = 1#1 & is_ori;
    reg_out_operand1 = 0#3 & is_ori;
    immediate_out_operand2 = 1#1 & is_ori;

    reg_in_valid = 1#1 & is_or;
    reg_in = instruction[3:0] & is_or;
    reg_out_operand1_valid = 1#1 & is_or;
    reg_out_operand1 = instruction[6:3] & is_or;
    reg_out_operand2_valid = 1#1 & is_or;
    reg_out_operand2 = instruction[9:6] & is_or;

    // xor, xori
    SR_zero_in = 1#1 & (is_xor | is_xori);
    alu_fn = 2#3 & (is_xor | is_xori);
    alu_out = 1#1 & (is_xor | is_xori);

    reg_in_valid = 1#1 & is_xori;
    reg_in = 0#3 & is_xori;
    reg_out_operand1_valid = 1#1 & is_xori;
    reg_out_operand1 = 0#3 & is_xori;
    immediate_out_operand2 = 1#1 & is_xori;

    reg_in_valid = 1#1 & is_xor;
    reg_in = instruction[3:0] & is_xor;
    reg_out_operand1_valid = 1#1 & is_xor;
    reg_out_operand1 = instruction[6:3] & is_xor;
    reg_out_operand2_valid = 1#1 & is_xor;
    reg_out_operand2 = instruction[9:6] & is_xor;

    // not
    SR_zero_in = 1#1 & is_not;
    alu_fn = 3#3 & is_not;
    alu_out = 1#1 & is_not;

    reg_in_valid = 1#1 & is_not;
    reg_in = instruction[3:0] & is_not;
    reg_out_operand1_valid = 1#1 & is_not;
    reg_out_operand1 = instruction[6:3] & is_not;

    // add, addi
    SR_carry_in = 1#1 & (is_add | is_addi);
    SR_zero_in = 1#1 & (is_add | is_addi);
    alu_fn = 6#3 & (is_add | is_addi);
    alu_out = 1#1 & (is_add | is_addi);

    reg_in_valid = 1#1 & is_addi;
    reg_in = 0#3 & is_addi;
    reg_out_operand1_valid = 1#1 & is_addi;
    reg_out_operand1 = 0#3 & is_addi;
    immediate_out_operand2 = 1#1 & is_addi;

    reg_in_valid = 1#1 & is_add;
    reg_in = instruction[3:0] & is_add;
    reg_out_operand1_valid = 1#1 & is_add;
    reg_out_operand1 = instruction[6:3] & is_add;
    reg_out_operand2_valid = 1#1 & is_add;
    reg_out_operand2 = instruction[9:6] & is_add;

    // sub, subi
    SR_carry_in = 1#1 & (is_sub | is_subi);
    SR_zero_in = 1#1 & (is_sub | is_subi);
    alu_fn = 7#3 & (is_sub | is_subi);
    alu_out = 1#1 & (is_sub | is_subi);

    reg_in_valid = 1#1 & is_subi;
    reg_in = 0#3 & is_subi;
    reg_out_operand1_valid = 1#1 & is_subi;
    reg_out_operand1 = 0#3 & is_subi;
    immediate_out_operand2 = 1#1 & is_subi;

    reg_in_valid = 1#1 & is_sub;
    reg_in = instruction[3:0] & is_sub;
    reg_out_operand1_valid = 1#1 & is_sub;
    reg_out_operand1 = instruction[6:3] & is_sub;
    reg_out_operand2_valid = 1#1 & is_sub;
    reg_out_operand2 = instruction[9:6] & is_sub;

    // sl, sli
    SR_zero_in = 1#1 & (is_sl | is_sli);
    alu_fn = 4#3 & (is_sl | is_sli);
    alu_out = 1#1 & (is_sl | is_sli);

    reg_in_valid = 1#1 & is_sli;
    reg_in = 0#3 & is_sli;
    reg_out_operand1_valid = 1#1 & is_sli;
    reg_out_operand1 = 0#3 & is_sli;
    immediate_out_operand2 = 1#1 & is_sli;

    reg_in_valid = 1#1 & is_sl;
    reg_in = instruction[3:0] & is_sl;
    reg_out_operand1_valid = 1#1 & is_sl;
    reg_out_operand1 = instruction[6:3] & is_sl;
    reg_out_operand2_valid = 1#1 & is_sl;
    reg_out_operand2 = instruction[9:6] & is_sl;

    // sr, sri
    SR_zero_in = 1#1 & (is_sr | is_sri);
    alu_fn = 5#3 & (is_sr | is_sri);
    alu_out = 1#1 & (is_sr | is_sri);

    reg_in_valid = 1#1 & is_sri;
    reg_in = 0#3 & is_sri;
    reg_out_operand1_valid = 1#1 & is_sri;
    reg_out_operand1 = 0#3 & is_sri;
    immediate_out_operand2 = 1#1 & is_sri;

    reg_in_valid = 1#1 & is_sr;
    reg_in = instruction[3:0] & is_sr;
    reg_out_operand1_valid = 1#1 & is_sr;
    reg_out_operand1 = instruction[6:3] & is_sr;
    reg_out_operand2_valid = 1#1 & is_sr;
    reg_out_operand2 = instruction[9:6] & is_sr;

    // cmp, cmpi
    SR_equal_in = 1#1 & (is_cmp | is_cmpi);
    SR_less_than_in = 1#1 & (is_cmp | is_cmpi);
    alu_fn = 7#3 & (is_cmp | is_cmpi);

    reg_out_operand1_valid = 1#1 & is_cmpi;
    reg_out_operand1 = 0#3 & is_cmpi;
    immediate_out_operand2 = 1#1 & is_cmpi;

    reg_out_operand1_valid = 1#1 & is_cmp;
    reg_out_operand1 = instruction[6:3] & is_cmp;
    reg_out_operand2_valid = 1#1 & is_cmp;
    reg_out_operand2 = instruction[9:6] & is_cmp;

    // jmp
    jmp_cond = instruction[3:0];
    jmp_is_link = instruction[6];

    jump_condition = jmp_cond & is_jmp;

    reg_in_valid = 1#1 & is_jmp;
    reg_in = 7#3 & is_jmp;

    RAddr_in_PC = jmp_is_link & is_jmp;

    reg_out_valid = 1#1 & is_jmp;
    reg_out = instruction[6:3] & is_jmp;
}
