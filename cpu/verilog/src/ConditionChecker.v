module ConditionChecker(
    input [2:0] condition,
    input carry_flag,
    input zero_flag,
    input equal_flag,
    input less_than_flag,

    output met);

    always @(*)
        case (condition)
            // No condition
            3'b000: met <= 1;
            // Zero
            3'b001: met <= zero_flag;
            // Equal
            3'b010: met <= equal_flag;
            // Not equal
            3'b011: met <= !equal_flag;
            // Less than
            3'b100: met <= less_than_flag;
            // Less than or equal
            3'b101: met <= less_than_flag || equal_flag;
            // Greater than
            3'b110: met <= !less_than_flag && !equal_flag;
            // Greater than or equal
            3'b111: met <= !less_than_flag;
        endcase

endmodule