module CPU(
    input clk,
    input rst,

    input mem_data_in,

    output mem_data_out,
    output mem_addr,
    output mem_enable,
    output mem_output_enable,
    output mem_write_enable,
    output mem_write_clk);

    wire [15:0] data_bus;
    wire [15:0] addr_bus;

    // TODO: connect modules

    Register R0(
        .clk(clk),
        .rst(rst),
        .value_in(data_bus),
        .load(),
        .value());

    Register R1(
        .clk(clk),
        .rst(rst),
        .value_in(data_bus),
        .load(),
        .value());

    Register R2(
        .clk(clk),
        .rst(rst),
        .value_in(data_bus),
        .load(),
        .value());

    Register Scratch(
        .clk(clk),
        .rst(rst),
        .value_in(data_bus),
        .load(),
        .value());

    Register SP(
        .clk(clk),
        .rst(rst),
        .value_in(data_bus),
        .load(),
        .value());

    Register RAddr(
        .clk(clk),
        .rst(rst),
        .value_in(data_bus),
        .load(),
        .value());

    StatusRegister SR(
        .clk(clk),
        .rst(rst),
        .value_in(data_bus),
        .load(),
        .value());

    ProgramCounter PC(
        .clk(clk),
        .rst(rst),
        .value_in(data_bus),
        .load(),
        .value());

    Register IR(
        .clk(clk),
        .rst(rst),
        .value_in(data_bus),
        .load(),
        .value());

    ALU alu(

        );

endmodule