module ProgramCounter(
    input clk,
    input rst,

    input [15:0] value_in,
    input enable_count,
    input load,

    output reg [15:0] value);

    always @(posedge clk) begin
        if (rst)
            value <= 0;
        else if (load)
            value <= value_in;
        else if (enable_count)
            value <= value + 1;
    end

endmodule