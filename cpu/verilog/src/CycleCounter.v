module CycleCounter(
    input clk,
    input rst,

    output reg is_fetch,
    output reg is_execute);

    always @(posedge clk) begin
        if (rst) begin
            is_fetch <= 1;
            is_execute <= 0;
        end else begin
            if (is_fetch) begin
                is_fetch <= 0;
                is_execute <= 1;
            end else begin
                is_fetch <= 1;
                is_execute <= 0;
            end
        end
    end

endmodule