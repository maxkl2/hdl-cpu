module InstructionDecoder(
    input [16] instruction,
    output [10:0] immediate_value,
    output reg_in_valid,
    output [2:0] reg_in,
    output RAddr_in_PC,
    output SR_carry_in, SR_zero_in, SR_equal_in, SR_less_than_in,
    output immediate_out,
    output reg_out_valid,
    output [2:0] reg_out,
    output reg_out_addr_valid,
    output [2:0] reg_out_addr,
    output reg_out_operand1_valid,
    output [2:0] reg_out_operand1,
    output immediate_out_operand2,
    output reg_out_operand2_valid,
    output [2:0] reg_out_operand2,
    output [2:0] jump_condition,
    output alu_out,
    output [2:0] alu_fn,
    output mem_read, mem_write);

    wire [4:0] opcode;
    assign opcode = instruction[15:11];

    assign immediate_value = instruction[10:0];

    always @(*) begin
        reg_in_valid <= 0;
        reg_in <= 0;
        RAddr_in_PC <= 0;
        SR_carry_in <= 0;
        SR_zero_in <= 0;
        SR_equal_in <= 0;
        SR_less_than_in <= 0;
        immediate_out <= 0;
        reg_out_valid <= 0;
        reg_out <= 0;
        reg_out_addr_valid <= 0;
        reg_out_addr <= 0;
        reg_out_operand1_valid <= 0;
        reg_out_operand1 <= 0;
        immediate_out_operand2 <= 0;
        reg_out_operand2_valid <= 0;
        reg_out_operand2 <= 0;
        jump_condition <= 0;
        alu_out <= 0;
        alu_fn <= 0;
        mem_read <= 0;
        mem_write <= 0;

        case (opcode)
            5'b00000: // mov
                reg_in_valid <= 1;
                reg_in <= instruction[2:0];
                reg_out_valid <= 1;
                reg_out <= instruction[5:3];
            5'b00001: // ld
                mem_read <= 1;

                reg_in_valid <= 1;
                reg_in <= instruction[2:0];
                reg_out_addr_valid <= 1;
                reg_out_addr <= instruction[5:3];
            5'b00010: // ldi
                reg_in_valid <= 1;
                reg_in <= 3'd0;
                immediate_out <= 1;
            5'b00011: // st
                mem_write <= 1;

                reg_out_addr_valid <= 1;
                reg_out_addr <= instruction[2:0];
                reg_out_valid <= 1;
                reg_out <= instruction[5:3];
            5'b00100: // and
                SR_zero_in <= 1;
                alu_fn <= 3'd0;
                alu_out <= 1;

                reg_in_valid <= 1;
                reg_in <= instruction[2:0];
                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= instruction[5:3];
                reg_out_operand2_valid <= 1;
                reg_out_operand2 <= instruction[8:6];
            5'b00101: // andi
                SR_zero_in <= 1;
                alu_fn <= 3'd0;
                alu_out <= 1;

                reg_in_valid <= 1;
                reg_in <= 3'd0;
                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= 3'd0;
                immediate_out_operand2 <= 1;
            5'b00110: // or
                SR_zero_in <= 1;
                alu_fn <= 3'd1;
                alu_out <= 1;

                reg_in_valid <= 1;
                reg_in <= instruction[2:0];
                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= instruction[5:3];
                reg_out_operand2_valid <= 1;
                reg_out_operand2 <= instruction[8:6];
            5'b00111: // ori
                SR_zero_in <= 1;
                alu_fn <= 3'd1;
                alu_out <= 1;

                reg_in_valid <= 1;
                reg_in <= 3'd0;
                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= 3'd0;
                immediate_out_operand2 <= 1;
            5'b01000: // xor
                SR_zero_in <= 1;
                alu_fn <= 3'd2;
                alu_out <= 1;

                reg_in_valid <= 1;
                reg_in <= instruction[2:0];
                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= instruction[5:3];
                reg_out_operand2_valid <= 1;
                reg_out_operand2 <= instruction[8:6];
            5'b01001: // xori
                SR_zero_in <= 1;
                alu_fn <= 3'd2;
                alu_out <= 1;

                reg_in_valid <= 1;
                reg_in <= 3'd0;
                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= 3'd0;
                immediate_out_operand2 <= 1;
            5'b01010: // not
                SR_zero_in <= 1;
                alu_fn <= 3'd3;
                alu_out <= 1;

                reg_in_valid <= 1;
                reg_in <= instruction[2:0];
                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= instruction[5:3];
            5'b01011: // add
                SR_carry_in <= 1;
                SR_zero_in <= 1;
                alu_fn <= 3'd6;
                alu_out <= 1;

                reg_in_valid <= 1;
                reg_in <= instruction[2:0];
                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= instruction[5:3];
                reg_out_operand2_valid <= 1;
                reg_out_operand2 <= instruction[8:6];
            5'b01100: // addi
                SR_carry_in <= 1;
                SR_zero_in <= 1;
                alu_fn <= 3'd6;
                alu_out <= 1;

                reg_in_valid <= 1;
                reg_in <= 3'd0;
                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= 3'd0;
                immediate_out_operand2 <= 1;
            5'b01101: // sub
                SR_carry_in <= 1;
                SR_zero_in <= 1;
                alu_fn <= 3'd7;
                alu_out <= 1;

                reg_in_valid <= 1;
                reg_in <= instruction[2:0];
                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= instruction[5:3];
                reg_out_operand2_valid <= 1;
                reg_out_operand2 <= instruction[8:6];
            5'b01110: // subi
                SR_carry_in <= 1;
                SR_zero_in <= 1;
                alu_fn <= 3'd7;
                alu_out <= 1;

                reg_in_valid <= 1;
                reg_in <= 3'd0;
                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= 3'd0;
                immediate_out_operand2 <= 1;
            5'b01111: // sl
                SR_zero_in <= 1;
                alu_fn <= 3'd4;
                alu_out <= 1;

                reg_in_valid <= 1;
                reg_in <= instruction[2:0];
                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= instruction[5:3];
                reg_out_operand2_valid <= 1;
                reg_out_operand2 <= instruction[8:6];
            5'b10000: // sli
                SR_zero_in <= 1;
                alu_fn <= 3'd4;
                alu_out <= 1;

                reg_in_valid <= 1;
                reg_in <= 3'd0;
                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= 3'd0;
                immediate_out_operand2 <= 1;
            5'b10001: // sr
                SR_zero_in <= 1;
                alu_fn <= 3'd5;
                alu_out <= 1;

                reg_in_valid <= 1;
                reg_in <= instruction[2:0];
                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= instruction[5:3];
                reg_out_operand2_valid <= 1;
                reg_out_operand2 <= instruction[8:6];
            5'b10010: // sri
                SR_zero_in <= 1;
                alu_fn <= 3'd5;
                alu_out <= 1;

                reg_in_valid <= 1;
                reg_in <= 3'd0;
                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= 3'd0;
                immediate_out_operand2 <= 1;
            5'b10011: // cmp
                SR_equal_in <= 1;
                SR_less_than_in <= 1;
                alu_fn <= 3'd7;

                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= instruction[5:3];
                reg_out_operand2_valid <= 1;
                reg_out_operand2 <= instruction[8:6];
            5'b10100: // cmpi
                SR_equal_in <= 1;
                SR_less_than_in <= 1;
                alu_fn <= 3'd7;

                reg_out_operand1_valid <= 1;
                reg_out_operand1 <= 3'd0;
                immediate_out_operand2 <= 1;
            5'b10101: // jmp
                jmp_cond <= instruction[2:0];
                jmp_is_link <= instruction[6];

                jump_condition <= jmp_cond;

                reg_in_valid <= 1;
                reg_in <= 3'd7;

                RAddr_in_PC <= jmp_is_link;

                reg_out_valid <= 1;
                reg_out <= instruction[5:3];
        endcase
    end

endmodule