module Shifter_tb;

   initial begin
      $dumpfile("wave.vcd");
      $dumpvars(0, Shifter_tb);

      #1000 $finish;
   end

   // 100 MHz clock
   reg clk = 0;
   always #5 clk = !clk;

   reg [4:0] ctr = 0;
   always @(posedge clk) ctr <= ctr + 1;

   wire [15:0] result;
   Shifter shifter(16'h1234, ctr[3:0], ctr[4], result);

   initial
      $monitor("At time %t, ctr: %d, result: %h / %b", $time, ctr, result, result);

endmodule