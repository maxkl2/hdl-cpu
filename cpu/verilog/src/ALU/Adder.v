module AdderWithCarry(
    input [15:0] operand1,
    input [15:0] operand2,
    input carry_in,

    output [15:0] result,
    output carry_out);

    wire [16:0] result_with_carry;

    assign result_with_carry = {1'b0, operand1} + {1'b0, operand2} + {16'b0, carry_in};

    assign result = result_with_carry[15:0];
    assign carry_out = result_with_carry[15];

endmodule

module AdderWithSubtract(
    input [15:0] operand1,
    input [15:0] operand2,
    input subtract,

    output [15:0] result,
    output carry_out);

    AdderWithCarry adder(
        .operand1(operand1),
        .operand2(subtract ? ~operand2 : operand2),
        .carry_in(subtract),
        .result(result),
        .carry_out(carry_out));

endmodule