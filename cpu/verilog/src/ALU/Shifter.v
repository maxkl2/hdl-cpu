module Shift(
    input [15:0] operand,
    input enable,
    input shift_right,

    output reg [15:0] result);

    parameter AMOUNT = 1;

    always @(*) begin
        if (enable) begin
            if (shift_right)
                result <= {{AMOUNT{1'b0}}, operand[15:AMOUNT]};
            else
                result <= {operand[15-AMOUNT:0], {AMOUNT{1'b0}}};
        end else
            result <= operand;
    end

endmodule

// This is a logarithmic shifter
// It has 4 stages that each shift by a power of two (1, 2, 4 and 8)
// Each stage is enabled if the corresponding bit in `bits` is set
module Shifter(
    input [15:0] operand,
    input [3:0] bits,
    input shift_right,

    output [15:0] result);

    wire [15:0] result1, result2, result4;

    Shift #(1) shift1(
        .operand(operand),
        .enable(bits[0]),
        .shift_right(shift_right),
        .result(result1));

    Shift #(2) shift2(
        .operand(result1),
        .enable(bits[1]),
        .shift_right(shift_right),
        .result(result2));

    Shift #(4) shift4(
        .operand(result2),
        .enable(bits[2]),
        .shift_right(shift_right),
        .result(result4));

    Shift #(8) shift8(
        .operand(result4),
        .enable(bits[3]),
        .shift_right(shift_right),
        .result(result));

endmodule