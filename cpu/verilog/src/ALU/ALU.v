module ALU(
    input [15:0] operand1,
    input [15:0] operand2,
    input [2:0] fn,

    output [15:0] result,
    output carry_flag,
    output zero_flag,
    output equal_flag,
    output less_than_flag);

    wire [15:0] result_shifter;
    wire [15:0] result_adder;

    // Function encoding:
    // 0x0: AND
    // 0x1: OR
    // 0x2: XOR
    // 0x3: NOT
    // 0x4: Shift left
    // 0x5: Shift right
    // 0x6: Add
    // 0x7: Subtract (also used for comparison)
    always @(*)
        case (fn)
            3'b000: result <= operand1 & operand2;
            3'b001: result <= operand1 | operand2;
            3'b010: result <= operand1 ^ operand2;
            3'b011: result <= ~operand1;
            3'b100: result <= result_shifter;
            3'b101: result <= result_shifter;
            3'b110: result <= result_adder;
            3'b111: result <= result_adder;
        endcase

    Shifter shifter(
        .operand(operand1),
        .bits(operand2[3:0]),
        .shift_right(fn == 3'b101),
        .result(result_shifter));

    AdderWithSubtract adder(
        .operand1(operand1),
        .operand2(operand2),
        .subtract(fn == 3'b111),
        .result(result_adder),
        .carry_out(carry_flag));

    assign zero_flag = result == 0;

    assign equal_flag = zero_flag;

    assign less_than_flag = !(equal_flag || carry_flag);

endmodule