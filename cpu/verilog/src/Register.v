module Register(
    input clk,
    input rst,

    input [15:0] value_in,
    input load,

    output reg [15:0] value);

    always @(posedge clk) begin
        if (rst)
            value <= 0;
        else if (load)
            value <= value_in;
    end

endmodule