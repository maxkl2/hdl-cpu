module RegisterDecoder(
    input [2:0] index,

    output is_R0,
    output is_R1,
    output is_R2,
    output is_Scratch,
    output is_SP,
    output is_RAddr,
    output is_SR,
    output is_PC);

    assign is_R0 = index == 0;
    assign is_R1 = index == 1;
    assign is_R2 = index == 2;
    assign is_Scratch = index == 3;
    assign is_SP = index == 4;
    assign is_RAddr = index == 5;
    assign is_SR = index == 6;
    assign is_PC = index == 7;

endmodule