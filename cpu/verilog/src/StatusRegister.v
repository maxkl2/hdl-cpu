module MaskedLoadRegister(
    input clk,
    input rst,

    input [15:0] value_in,
    input [15:0] load_mask,
    input load,

    output value);

    always @(posedge clk) begin
        if (rst)
            value <= 0;
        else if (load) begin
            value <= (value_in & load_mask) | (value & ~load_mask);
        end
    end

endmodule

module StatusRegister(
    input clk,
    input rst,

    input [15:0] value_in,
    input value_in_carry,
    input value_in_Z,
    input value_in_EQ,
    input value_in_LT,
    input load,
    input load_carry,
    input load_Z,
    input load_EQ,
    input load_LT,

    output [15:0] value,
    output value_carry,
    output value_Z,
    output value_EQ,
    output value_LT);

    wire [15:0] mask_flags;
    wire [15:0] mask;
    wire [15:0] value_in_merged;

    assign mask_flags = {12'b0, load_LT, load_EQ, load_Z, load_carry};
    assign mask = load ? 16'hffff : mask_flags;
    assign value_in_merged = {
        12'b0,
        value_in_LT & load_LT,
        value_in_EQ & load_EQ,
        value_in_Z & load_Z,
        value_in_carry & load_carry
    } | (value_in & ~mask_flags);

    assign value_carry = value[0];
    assign value_Z = value[1];
    assign value_EQ = value[2];
    assign value_LT = value[3];

    MaskedLoadRegister reg_internal(
        .clk(clk),
        .rst(rst),

        .value_in(value_in_merged),
        .load_mask(mask),
        .load(load || load_carry || load_Z || load_EQ || load_LT),

        .value(value));

endmodule